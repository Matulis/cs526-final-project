# A custom container and attach it to the camera as a 3d head-up display
from omegaToolkit import *
uim = UiModule.createAndInitialize()
wf = uim.getWidgetFactory()

hud = Container.create(ContainerLayout.LayoutVertical, uim.getUi())
hud.setStyle('fill: #00000080')
hud.setClippingEnabled(True)
l1 = Label.create(hud)
l2 = Label.create(hud)
l3 = Label.create(hud)

planet = loadImage('planet_textures/earth.png')

widgetImage = wf.createImage('earth',hud)

widgetImage.setData(planet)
widgetImage.setSize(Vector2(1000,1000))

l1.setFont('fonts/arial.ttf 20')
l1.setText("Heads up display test")

l2.setFont('fonts/arial.ttf 14')
l2.setText("Camera position:")

l3.setFont('fonts/arial.ttf 14')

# enable 3d mode for the hud container and attach it to the camera.
c3d = hud.get3dSettings()
#c3d.enable3d = True
c3d.position = Vector3(-2, 2.5, -6.5)
# Rotate the hud a little. Note that rotation needs to be specified 
# as a vector.
c3d.normal = quaternionFromEulerDeg(0,-30,0) * Vector3(0,0,1)
# Scale is the conversion factor between pixels and meters
c3d.scale = 0.004
c3d.node = getDefaultCamera()


def onUpdate(frame, time, dt):
    l3.setText(str(getDefaultCamera().getPosition()))

# Add keys to remove / add the entity to the clip plane
def onEvent():
    e = getEvent()
    if(e.isKeyDown(ord('o'))):
        size = widgetImage.getScale()
        size *= 1.1
        widgetImage.setScale(size)
    if(e.isKeyDown(ord('l'))):
        size = widgetImage.getScale()
        size *= 0.9
        widgetImage.setScale(size)

setEventFunction(onEvent)

setUpdateFunction(onUpdate)