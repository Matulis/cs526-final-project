
from math import *
from euclid import *
from omega import *
from cyclops import *

from omegaToolkit import *
uim = UiModule.createAndInitialize()
wf = uim.getWidgetFactory()

hud = Container.create(ContainerLayout.LayoutFree, uim.getUi())
hud.setStyle('fill: #00000080')
hud.setClippingEnabled(True)
hud.setAutosize(False)
hud.setSize(Vector2(1000,1000))


star = Container.create(ContainerLayout.LayoutFree,hud)
star.setStyle('fill: #FF0000')
star.setSize(Vector2(200,200))
star.setAutosize(False)

star2 = Container.create(ContainerLayout.LayoutFree,hud)
star2.setStyle('fill: #AA0000')
star2.setSize(Vector2(200,200))
star2.setAutosize(False)
star2.setPosition(Vector2(100,200))

star3 = Container.create(ContainerLayout.LayoutFree,hud)
star3.setStyle('fill: #AAAA00')
star3.setSize(Vector2(200,200))
star3.setAutosize(False)
star3.setPosition(Vector2(100,0))


# Add keys to remove / add the entity to the clip plane
def onEvent():
    e = getEvent()
    if(e.isKeyDown(ord('o'))):
        size = star3.getScale()
        size *= 1.1
        star3.setScale(size)
    if(e.isKeyDown(ord('l'))):
        size = star3.getScale()
        size *= 0.9
        star3.setScale(size)

setEventFunction(onEvent)