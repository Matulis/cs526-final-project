
from math import *
from euclid import *
from omega import *
from cyclops import *

from omegaToolkit import *

from systemClasses import *
from systemParse import *

from graphContainer import *

from utilities import *

from textureSet import *
from multiplesContainer import *

from CoordinateCalculator import CoordinateCalculator

#Setup small multiples containers:
displaySize = getDisplayPixelSize()

mm = MenuManager.createAndInitialize()

xAxisMenu = None
yAxisMenu = None

def createXLabelMenu():
    global xAxisMenu
    xAxisMenu = mm.createMenu("xAxisMenu")
    xAxisMenu.addButton("Radius","graphMultiple.XString = \"radius\";graphMultiple.plotObjects()")
    xAxisMenu.addButton("Mass","graphMultiple.XString =\"mass\";graphMultiple.plotObjects()")
    xAxisMenu.addButton("Orbit Period","graphMultiple.XString =\"period\";graphMultiple.plotObjects()")
    xAxisMenu.addButton("Distance From Star","graphMultiple.XString =\"semimajoraxis\";graphMultiple.plotObjects()")
    xAxisMenu.addButton("Eccentricity","graphMultiple.XString =\"eccentricity\";graphMultiple.plotObjects()")
    xAxisMenu.addButton("System Distance From Earth","graphMultiple.XString =\"distance\";graphMultiple.plotObjects()")
    xAxisMenu.addButton("Planet Discovery Method","graphMultiple.XString =\"discovery\";graphMultiple.plotObjects()")
    
#Stellar magnitude "Star type with enun?"
#Discoveryt method, add enum?

#Distance from our sun

crosshair = None

#Based on Thomas Marrinan Laser Pointer
def loadCrosshair():
    global crosshair
    crosshair = Image.create(uim.getUi())
    crosshair.setData(loadImage("crosshair.png"))
    crosshair.setSize(Vector2(150,150))
    crosshair.setCenter(Vector2(150,150))


def createYLabelMenu():
    global yAxisMenu
    yAxisMenu = mm.createMenu("yAxisMenu")
    yAxisMenu.addButton("Radius","graphMultiple.YString = \"radius\";graphMultiple.plotObjects()")
    yAxisMenu.addButton("Mass","graphMultiple.YString = \"mass\";graphMultiple.plotObjects()")
    yAxisMenu.addButton("Orbit Period","graphMultiple.YString =\"period\";graphMultiple.plotObjects()")
    yAxisMenu.addButton("Distance From Star","graphMultiple.YString =\"semimajoraxis\";graphMultiple.plotObjects()")
    yAxisMenu.addButton("Eccentricity","graphMultiple.YString =\"eccentricity\";graphMultiple.plotObjects()")
    yAxisMenu.addButton("System Distance From Earth","graphMultiple.YString =\"distance\";graphMultiple.plotObjects()")
    yAxisMenu.addButton("Planet Discovery Method","graphMultiple.YString =\"discovery\";graphMultiple.plotObjects()")
    


AUTOKM = 149597871

#CONSTANTS
DISPLAYWIDTH = 18
DISPLAYHEIGHT = 4

smallMultipleWidth = 2.0
smallMultipleHeight = 0.5

#Fraction of container that should be star
starContainerFraction = 0.05

CurrentlySelectedContainer = None


#SMALL MULTIPLE SCALE CONSTANTS/GLOBALS
ORBITSCALE = 44.0 #Pluto semimajor
SIZESCALE = 4.0 #Jupiter radius

sm_currentOrbitScale = float(ORBITSCALE)
sm_currentSizeScale = float(SIZESCALE)


##### Planet sphere model
scene = getSceneManager()

scene.setBackgroundColor(Color(0, 0, 0, 1))

mi = ModelInfo()
mi.name = "defaultSphere"
mi.path = "sphere.obj"
scene.loadModel(mi)


print "MODADFSDFSDFDF IS LOADED!!!!!!!!!!!!!!"

######Center system node
centerSystem = SceneNode.create('centerSystem')
everything = SceneNode.create('everything')

everything.addChild(centerSystem)


# Create point light for star
light1 = Light.create()
light1.setLightType(LightType.Point)
light1.setColor(Color(1.0, 1.0, 1.0, 1.0))
light1.setPosition(Vector3(0.0, 0.0, 0.0))
light1.setEnabled(True)

everything.addChild(light1)

print displaySize
uim = UiModule.createAndInitialize()
wf = uim.getWidgetFactory()

uim.getUi().setPadding(0)

#Master container
masterMultiples = Container.create(ContainerLayout.LayoutFree, uim.getUi())
masterMultiples.setAutosize(False)
masterMultiples.setSize(Vector2(displaySize[0],displaySize[1]))
masterMultiples.setPadding(0)
masterMultiples.setMargin(0)
masterMultiples.setLayer(WidgetLayer.Back)

#Left Multiples
leftMultiples = Container.create(ContainerLayout.LayoutHorizontal,masterMultiples)
leftMultiples.setPadding(1)
leftMultiples.setMargin(0)

#Left Multiples with graph part
leftMultiplesGraphHor = Container.create(ContainerLayout.LayoutHorizontal,leftMultiples)
leftMultiplesGraphHor.setPadding(1)
leftMultiplesGraphHor.setMargin(0)

leftMultiplesGraphVert = Container.create(ContainerLayout.LayoutVertical,leftMultiplesGraphHor)
leftMultiplesGraphVert.setPadding(1)
leftMultiplesGraphVert.setMargin(0)

graphContainer = Container.create(ContainerLayout.LayoutVertical,leftMultiplesGraphVert)
graphContainer.setPadding(1)
graphContainer.setMargin(0)
graphContainer.setClippingEnabled(False)
graphContainer.setAutosize(False)
graphContainer.setSize(Vector2((smallMultipleWidth*2/DISPLAYWIDTH)*displaySize[0], \
(smallMultipleHeight*3.5/DISPLAYHEIGHT)*displaySize[1]))

graphHorizontalContainer = Container.create(ContainerLayout.LayoutHorizontal,graphContainer)
graphHorizontalContainer.setPadding(1)
graphHorizontalContainer.setMargin(0)
graphHorizontalContainer.setWidth((smallMultipleWidth*2/DISPLAYWIDTH)*displaySize[0])
graphHorizontalContainer.setClippingEnabled(True)
graphHorizontalContainer.setAutosize(False)



yLabel = Label.create(graphHorizontalContainer)
yLabel.setFont('fonts/arial.ttf {0}'.format(50))
#yLabel.setText("Value")
#yLabel.setRotation(-90)
# Rotation does not allow for the hit test to work....

graphMultiple = GraphContainer.create(ContainerLayout.LayoutFree,graphHorizontalContainer)
graphMultiple.container.setClippingEnabled(True)
graphMultiple.container.setAutosize(False)
graphMultiple.container.setSize(Vector2((smallMultipleWidth*2/DISPLAYWIDTH)*displaySize[0], \
(smallMultipleHeight*3.5/DISPLAYHEIGHT)*displaySize[1]))
graphMultiple.container.setStyle('fill: #1111FF30')


xLabel = Label.create(leftMultiplesGraphVert)
xLabel.setFont('fonts/arial.ttf {0}'.format(50))
#xLabel.setText("THIS IS A TEST OF THE TEST")


graphMultiple.xLabel = xLabel
graphMultiple.yLabel = yLabel


createXLabelMenu()
createYLabelMenu()

leftMultiplesUnderGraph = Container.create(ContainerLayout.LayoutHorizontal,leftMultiplesGraphVert)
leftMultiplesUnderGraph.setPadding(1)
leftMultiplesUnderGraph.setMargin(0)


rightMultiples = Container.create(ContainerLayout.LayoutHorizontal,masterMultiples)
rightMultiples.setPadding(1)
rightMultiples.setMargin(0)
rightMultiples.setPosition(Vector2((DISPLAYWIDTH-3*smallMultipleWidth)/DISPLAYWIDTH*displaySize[0],0))

multiplesSides = [leftMultiples,rightMultiples]

allSmallMultiples = []

#container for graphs



for side in multiplesSides:

    for i in xrange(0,3):
        column = None
        #free up left two vertical containers
        numberOfMultiples = 8
        if(side == leftMultiples):
            column = Container.create(ContainerLayout.LayoutVertical,leftMultiplesUnderGraph)
            numberOfMultiples = 4
        else:
            column = Container.create(ContainerLayout.LayoutVertical,side)


        column.setAutosize(False)
        column.setSize(Vector2((smallMultipleWidth/DISPLAYWIDTH)*displaySize[0],displaySize[1]))
        column.setPadding(1)
        column.setMargin(0)
        column.setPosition(Vector2(0,0))
        for i in xrange(0,numberOfMultiples):
            smallMultiple = MultiplesContainer.create(ContainerLayout.LayoutFree,column)
            smallMultiple.container.setClippingEnabled(True)
            allSmallMultiples.append(smallMultiple)
            smallMultiple.container.setAutosize(False)
            smallMultiple.container.setSize(Vector2((smallMultipleWidth/DISPLAYWIDTH)*displaySize[0], \
                                   (smallMultipleHeight/DISPLAYHEIGHT)*displaySize[1]))
            smallMultiple.container.setStyle('fill: #11111180')

#Load textures
LoadStarTextures("./startextures")
LoadAllPlanetTextures("./sphere_textures","./multiples_textures")

#Import Solar System Data

universe = ParseAll("./system_data")

currentSystem = None

for system in universe.systems:
    if system.name == "Sun":
        currentSystem = system



def createSmallMultiple(parentContainer,system):
#Create sun
    global sm_currentSizeScale
    global sm_currentOrbitScale
    global ORBITSCALE
    global SIZESCALE
    
    parentContainer.system = system
    
    fontsize = 40 #FOR CAVE
#    fontsize = 10 #FOR LAPTOP
    parentSize = parentContainer.container.getSize()
    print system.name
    star = system.stars[0]
    starType = star.spectraltype
    
    starImage = Image.create(parentContainer.container)
    
    if starType.find('A')!=-1 :
        starImage.setData(StarTextures["astar"])
        parentContainer.innerHabitable = 8.5
        parentContainer.outerHabitable = 12.5
    elif starType.find('B')!=-1 :
        starImage.setData(StarTextures["bstar"])
        parentContainer.innerHabitable = None
        parentContainer.outerHabitable = None
    elif starType.find('F')!=-1 :
        starImage.setData(StarTextures["fstar"])
        parentContainer.innerHabitable = 1.5
        parentContainer.outerHabitable = 2.2
    elif starType.find('G')!=-1 :
        starImage.setData(StarTextures["gstar"])
        parentContainer.innerHabitable = 0.95
        parentContainer.outerHabitable = 1.4
    elif starType.find('K')!=-1 :
        starImage.setData(StarTextures["kstar"])
        parentContainer.innerHabitable = 0.38
        parentContainer.outerHabitable = 0.56
    elif starType.find('M')!=-1 :
        starImage.setData(StarTextures["mstar"])
        parentContainer.innerHabitable = 0.08
        parentContainer.outerHabitable = 0.12
    elif starType.find('O')!=-1 :
        starImage.setData(StarTextures["ostar"])
        parentContainer.innerHabitable = None
        parentContainer.outerHabitable = None
        

    starImage.setSize(Vector2(parentSize[0]*starContainerFraction,parentSize[1]))

    starOffset = starContainerFraction * parentSize[0]
    parentContainer.starOffset = starOffset

#Add container for habitable zone:
    parentContainer.habitableContainer = Container.create(ContainerLayout.LayoutFree,parentContainer.container)
    parentContainer.habitableContainer.setAutosize(False)
    parentContainer.habitableContainer.setSize(Vector2(((parentContainer.outerHabitable-parentContainer.innerHabitable)/sm_currentOrbitScale)*parentSize[0],parentSize[1]))
    parentContainer.habitableContainer.setStyle('fill: #00FF0080')
    parentContainer.habitableContainer.setPosition(Vector2((parentContainer.innerHabitable/sm_currentOrbitScale)*parentSize[0]+starOffset,0))

    

#Add labels for small multiples visualization
    textContainer = Container.create(ContainerLayout.LayoutHorizontal,parentContainer.container)
    textContainer.setPosition(Vector2(textContainer.getPosition()[0]+starOffset,textContainer.getPosition()[1]))
    textContainer.setHorizontalAlign(HAlign.AlignLeft)
    textContainer.setMargin(0)
    textContainer.setPadding(0)

    infoLabels = []
    systemName = Label.create(textContainer) #System name
    systemName.setText(system.name)
    infoLabels.append(systemName)

    distanceLabel = Label.create(textContainer) #System distance in lightyears
    try:
        distanceLabel.setText("{0:.2f} ly".format(system.distance*3.26163344)) #Convert to lightyears
        infoLabels.append(distanceLabel)
    except:
        distanceLabel.setText("") #Convert to lightyears
        infoLabels.append(distanceLabel)

    if(system.discoverymethod is not None):
        discoveryLabel = Label.create(textContainer)
        discoveryLabel.setText(system.discoverymethod)
        infoLabels.append(discoveryLabel)

    starLabel = Label.create(textContainer)
    starLabel.setText("Star: "+ starType)
    infoLabels.append(starLabel)

    for label in infoLabels:
        label.setFont('fonts/arial.ttf {0}'.format(fontsize))
        label.setStyleValue('align', 'top-left')
    

#Load planets
    
    for index,planet in enumerate(star.planets):
        planetImage = Image.create(parentContainer.container)
        planetImage.setData(planet.texture.multiplesTexture)
        planetImage.setSize(Vector2(parentSize[1],parentSize[1]))
        planetImage.setScale(planet.radius/sm_currentSizeScale)

        planetImage.setCenter(Vector2((planet.semimajoraxis/sm_currentOrbitScale)*parentSize[0]+starOffset,parentSize[1]/2))
        parentContainer.planets.append(planetImage)

        planetLabel = Label.create(parentContainer.container)
        planetLabel.setText(planet.name)
        planetLabel.setPosition(Vector2(planetImage.getCenter()[0], fontsize*2 if index%2 else parentSize[1]-fontsize*2))
        planetLabel.setFont('fonts/arial.ttf {0}'.format(fontsize))
        parentContainer.planetLabels.append(planetLabel)
    
#Initialize container scaling so it can keep track of it
        parentContainer.SizeScale = sm_currentSizeScale/SIZESCALE
        parentContainer.OrbitScale = sm_currentOrbitScale/ORBITSCALE


##############################################################################
#Create center system

centerSystem.addChild(currentSystem.setupCenterSystem())



###############################################################################

#Create small multiples menu
mm = MenuManager.createAndInitialize()
smMenu = mm.getMainMenu().addSubMenu("Small Multiples")
sizeScaleLabel = smMenu.addLabel("Size Scale")

#sizeUpButton = smMenu.addButton("+","scaleSize(1.01)")
#sizeDownButton = smMenu.addButton("-","scaleSize(0.99)")
#
#orbitScaleLabel = smMenu.addLabel("Orbit Scale")
#orbitUpButton = smMenu.addButton("+","scaleOrbit(1.01)")
#orbitDownButton = smMenu.addButton("+","scaleOrbit(0.99)")

sizeScaleSlider = smMenu.addSlider(22,"sizescale =pow(1.2 if %value%-11>0 else 0.8,abs(%value%-11)); scaleSize(sizescale)")
sizeScaleSlider.getSlider().setValue(11) #Half of slider
sizeScaleSlider.getSlider().setWidth(200)
orbitScaleLabel = smMenu.addLabel("Orbit Scale")
orbitScaleSlider = smMenu.addSlider(22,"oscale =pow(1.2 if %value%-11>0 else 0.8,abs(%value%-11));scaleOrbit(oscale)")
orbitScaleSlider.getSlider().setValue(11) #Half of slider
orbitScaleSlider.getSlider().setWidth(200)


resetButton = smMenu.addButton("Reset Small Multiples","resetSmallMultiples()")


#Create center system menu
cMenu = mm.getMainMenu().addSubMenu("Center System")
sizeScaleLabel = cMenu.addLabel("Size Scale")

#sizeUpButton = smMenu.addButton("+","scaleSize(1.01)")
#sizeDownButton = smMenu.addButton("-","scaleSize(0.99)")
#
#orbitScaleLabel = smMenu.addLabel("Orbit Scale")
#orbitUpButton = smMenu.addButton("+","scaleOrbit(1.01)")
#orbitDownButton = smMenu.addButton("+","scaleOrbit(0.99)")

sizeScaleSlider = cMenu.addSlider(22,"scale = pow(1.5,%value%-11); delete(centerSystem); centerSystem.addChild(currentSystem.setupCenterSystem(scale,None))")
sizeScaleSlider.getSlider().setValue(11) #Half of slider
sizeScaleSlider.getSlider().setWidth(200)
orbitScaleLabel = cMenu.addLabel("Orbit Scale")
orbitScaleSlider = cMenu.addSlider(22,"scale = pow(1.5,%value%-11); delete(centerSystem); centerSystem.addChild(currentSystem.setupCenterSystem(None,scale))")
orbitScaleSlider.getSlider().setValue(11) #Half of slider
orbitScaleSlider.getSlider().setWidth(200)

timeScaleLabel = cMenu.addLabel("Time Scale Slider")
timeScaleSlider = cMenu.addSlider(24,"timeScale = pow(2,%value%-12)")
timeScaleSlider.getSlider().setValue(12) #Half of slider
timeScaleSlider.getSlider().setWidth(200)

resetButton = cMenu.addButton("Reset Size+Orbit+Camera","resetCenterAndCamera()")

#Plot Menu
plotMenu = mm.getMainMenu().addSubMenu("Plot Menu")

infoBoxCheckbox = plotMenu.addButton("Display Info Log","toggleInfoBoxDisplay(%value%)")
infoBoxCheckbox.getButton().setCheckable(True)
infoBoxCheckbox.getButton().setChecked(False)


glyphMenu = plotMenu.addSubMenu("Glyph Menu")


glyphMenu.addLabel("Glyph Size")
glyphSizeSlider = glyphMenu.addSlider(20,"graphMultiple.setImageSize( pow(0.9 if %value%-10 < 0 else 1.1, abs(%value%-10)))")
glyphSizeSlider.getSlider().setWidth(200)
glyphSizeSlider.getSlider().setValue(10)


button = glyphMenu.addButton("Blue Sphere","graphMultiple.setGlyph(0)")
button.getButton().setIcon(Glyph.allGlyphs[0].image)
button.getButton().getImage().setSize(Vector2(GraphContainer.defaultSize,GraphContainer.defaultSize))

button = glyphMenu.addButton("Green Sphere", "graphMultiple.setGlyph(1)")
button.getButton().setIcon(Glyph.allGlyphs[1].image)
button.getButton().getImage().setSize(Vector2(GraphContainer.defaultSize,GraphContainer.defaultSize))

button = glyphMenu.addButton("Red Sphere", "graphMultiple.setGlyph(2)")
button.getButton().setIcon(Glyph.allGlyphs[2].image)
button.getButton().getImage().setSize(Vector2(GraphContainer.defaultSize,GraphContainer.defaultSize))

button = glyphMenu.addButton("Blue Square","graphMultiple.setGlyph(3)")
button.getButton().setIcon(Glyph.allGlyphs[3].image)
button.getButton().getImage().setSize(Vector2(GraphContainer.defaultSize,GraphContainer.defaultSize))

button = glyphMenu.addButton("Green Square", "graphMultiple.setGlyph(4)")
button.getButton().setIcon(Glyph.allGlyphs[4].image)
button.getButton().getImage().setSize(Vector2(GraphContainer.defaultSize,GraphContainer.defaultSize))

button = glyphMenu.addButton("Red Square", "graphMultiple.setGlyph(5)")
button.getButton().setIcon(Glyph.allGlyphs[5].image)
button.getButton().getImage().setSize(Vector2(GraphContainer.defaultSize,GraphContainer.defaultSize))


xAxisLogCheckbox = plotMenu.addButton("X Axis Log","graphMultiple.setXLog(%value%)")
xAxisLogCheckbox.getButton().setCheckable(True)
xAxisLogCheckbox.getButton().setChecked(False)


yAxisLogCheckbox = plotMenu.addButton("Y Axis Log","graphMultiple.setYLog(%value%)")
yAxisLogCheckbox.getButton().setCheckable(True)
yAxisLogCheckbox.getButton().setChecked(False)


displayInfoBox = False

def toggleInfoBoxDisplay(value):
    global displayInfoBox
    displayInfoBox =  True if value > 0 else False


def checkBoxTest(value):
    print value
    
universeTime = 0
timeScale = 1

def onUpdate(frame, t, dt):
    global universeTime
    global timeScale
    universeTime += dt*timeScale
    if centerSystem.numChildren()>0:
        for planet in currentSystem.stars[0].planets:
            try:
                pos = get2DPositionofPlanet(planet,universeTime)
                planet.sceneGraphNode.setPosition(Vector3(pos[0],0,pos[1]))
                if planet.day != None:
                    planet.modelNode.yaw(math.radians(dt*timeScale*planet.day))
            except: pass

            
setUpdateFunction(onUpdate)

def areAllPlanetsVisible(multiples_container):
    allPlanetsVisible = True
    for planet in multiples_container.planets:
        if(planet.getCenter()[0] > multiples_container.container.getSize()[0]):
            allPlanetsVisible = False
            
    if allPlanetsVisible is True:
        multiples_container.container.setStyleValue('border', '0 #0000FF')
    else:
        multiples_container.container.setStyleValue('border', '5 #0000FF')

def resetSmallMultiples():
    for container in allSmallMultiples:
        for planet in container.planets:
            try: float(container.SizeScale)
            except: container.SizeScale = 1
            planet.setScale(planet.getScale()*(1/container.SizeScale))
            planet.setCenter(Vector2(planet.getCenter()[0]*(1/container.OrbitScale),planet.getCenter()[1]))
        
        for label in container.planetLabels:
            label.setPosition(Vector2(label.getPosition()[0] * (1/container.OrbitScale),label.getPosition()[1]))
    
        if container.habitableContainer is not None:
            container.habitableContainer.setPosition(Vector2(container.habitableContainer.getPosition()[0]*(1/container.OrbitScale),0))
            container.habitableContainer.setSize(Vector2(container.habitableContainer.getSize()[0]*(1/container.OrbitScale),container.habitableContainer.getSize()[1]))

        areAllPlanetsVisible(container)
        container.SizeScale = 1
        container.OrbitScale = 1
            
    sm_currentSizeScale = SIZESCALE
    sm_currentOrbitScale = ORBITSCALE

def resetCenterAndCamera():
    global centerScaleFactor
    global centerOrbitFactor

    centerScaleFactor = 0.3
    centerOrbitFactor = 0.3
    centerSystem.setChildrenVisible(False)
    delete(centerSystem);
    centerSystem.addChild(currentSystem.setupCenterSystem(centerScaleFactor,centerOrbitFactor))

def updateCenter():
    centerSystem.setChildrenVisible(False)
    delete(centerSystem);
    centerSystem.addChild(currentSystem.setupCenterSystem(None,None))


#    getDefaultCamera().setPosition(Vector3(0,0,0))

#Move camera to default



def scaleOrbit(scale):
    global allSmallMultiples
    
    sm_currentOrbitScale = scale*ORBITSCALE
    for container in allSmallMultiples:
        print len(container.planets)
        for planet in container.planets:
            try: float(container.OrbitScale)
            except: container.OrbitScale = 1
            planet.setCenter(Vector2(planet.getCenter()[0] * (scale/container.OrbitScale),planet.getCenter()[1]))
        for label in container.planetLabels:
            label.setPosition(Vector2(label.getPosition()[0] * (scale/container.OrbitScale),label.getPosition()[1]))
    
        if container.habitableContainer is not None:
            container.habitableContainer.setSize(Vector2( (container.habitableContainer.getSize()[0]*(float(scale)/float(container.OrbitScale))),container.container.getSize()[1]))

            container.habitableContainer.setPosition(Vector2(container.habitableContainer.getPosition()[0]*(float(scale)/float(container.OrbitScale)),0))
#            container.habitableContainer.setPosition(Vector2( (container.innerHabitable*container.container.getSize()[0]/scale+container.starOffset,0))
            

        areAllPlanetsVisible(container)
        container.OrbitScale = float(scale)

#            parentContainer.habitableContainer.setSize(Vector2(((parentContainer.outerHabitable-parentContainer.innerHabitable)/sm_currentOrbitScale)*parentSize[0]+starOffset,parentSize[1]))
#                parentContainer.habitableContainer.setStyle('fill: #00FF0080')

def changeCurrentlySelectedContainer(container):
    global CurrentlySelectedContainer
    global currentSystem
    global graphMultiple
    
    if CurrentlySelectedContainer is not None:
        CurrentlySelectedContainer.container.setStyle('fill: #11111180')
    
    CurrentlySelectedContainer = container;
    CurrentlySelectedContainer.container.setStyle('fill: #1111FF30')
    
    currentSystem = CurrentlySelectedContainer.system
    graphMultiple.currentSystem = currentSystem
    
    graphMultiple.plotObjects()


def getContainerForSystem(system):
    global allSmallMultiples
    
    for container in allSmallMultiples:
        if(system == container.system):
            return container

def scaleSize(scale):
    sm_currentSizeScale = scale*SIZESCALE
    
    for container in allSmallMultiples:
        for planet in container.planets:
            try: float(container.SizeScale)
            except: container.SizeScale = 1
            planet.setScale(planet.getScale()*(scale/container.SizeScale))
        areAllPlanetsVisible(container)
        container.SizeScale = scale
        
            
graphInfoBox = Container.create(ContainerLayout.LayoutVertical, uim.getUi())
#graphInfoBox.setAutosize(False)
#graphInfoBox.setSize(Vector2(5000,5000))
#graphInfoBox.setStyleValue('align', 'top-left')

    
infoLabels = []
systemNameLabel = None
systemDistanceLabel = None
systemNumPlanetsLabel = None

planetNameLabel = None
planetMassLabel = None
planetRadiusLabel = None
planetPeriodLabel = None
planetSemiLabel = None
planetDiscoveryLabel = None


def createPlanetInfoBoxLabels():
    global systemNameLabel
    global systemDistanceLabel
    global systemNumPlanetsLabel
    
    global planetNameLabel
    global planetMassLabel
    global planetRadiusLabel
    global planetPeriodLabel
    global planetSemiLabel
    global planetDiscoveryLabel
    
    systemNameLabel = Label.create(graphInfoBox)
    infoLabels.append(systemNameLabel)
    
    systemDistanceLabel = Label.create(graphInfoBox)
    infoLabels.append(systemDistanceLabel)
    
    systemNumPlanetsLabel = Label.create(graphInfoBox)
    infoLabels.append(systemNumPlanetsLabel)
    
    planetNameLabel = Label.create(graphInfoBox)
    infoLabels.append(planetNameLabel)
    
    planetMassLabel = Label.create(graphInfoBox)
    infoLabels.append(planetMassLabel)
    
    planetRadiusLabel = Label.create(graphInfoBox)
    infoLabels.append(planetRadiusLabel)
    
    planetPeriodLabel = Label.create(graphInfoBox)
    infoLabels.append(planetPeriodLabel)
    
    planetSemiLabel = Label.create(graphInfoBox)
    infoLabels.append(planetSemiLabel)
    
    planetDiscoveryLabel = Label.create(graphInfoBox)
    infoLabels.append(planetDiscoveryLabel)
    
    for label in infoLabels:
        label.setFont('fonts/arial.ttf {0}'.format(50))
        label.setStyleValue('align', 'center-left')
    
    
    
createPlanetInfoBoxLabels()

    #When highlighting over a point create an info box with additional planet and system info
def updatePlanetInfoBox(point):
    global graphInfoBox
    #Create labels for all info
    systemNameLabel.setText("System: {0}".format(point.system.name))
    systemDistanceLabel.setText("System Distance: {0} parsec".format(point.system.distance))
    systemNumPlanetsLabel.setText("Number of Planets: {0}".format(len(point.system.stars[0].planets)))
    planetNameLabel.setText("Planet: {0}".format(point.planet.name))
    planetMassLabel.setText("Mass: {0} (JM)".format(point.planet.mass))
    planetRadiusLabel.setText("Radius: {0} (JR)".format(point.planet.radius))
    planetPeriodLabel.setText("Period: {0} days".format(point.planet.period))
    planetSemiLabel.setText("Semimajor Axis: {0} AU".format(point.planet.semimajoraxis))
    planetDiscoveryLabel.setText("Discovery Method: {0}".format(point.planet.discoverymethod))
    
    
#initialize planets

for index,container in enumerate(allSmallMultiples):
    if(currentSystem == universe.systems[index]):
        container.system = universe.systems[index]
        changeCurrentlySelectedContainer(container);
        
    createSmallMultiple(container,universe.systems[index])
    areAllPlanetsVisible(container)
    graphMultiple.addSystem(universe.systems[index])
    


graphMultiple.plotObjects()
    
    
#masterMultiples.setVisible(False)

loadCrosshair()

oscale = 1
sizescale = 1

####################  Music    ########################
playMusic = True
env = getSoundEnvironment()
env.setAssetDirectory("/home/evl/cs526/martin/Project2/music/")
if playMusic:
    music = env.loadSoundFromFile('music', 'track1.wav') #might need full path here?
    simusic = SoundInstance(music)
    simusic.setVolume(.2)
    simusic.setLoop(True)
    simusic.play()

moveSpeed = 40

# Add keys to remove / add the entity to the clip plane
def onEvent():
    global oscale
    global sizescale
    global timeScale
    global currentSystem
    global xAxisMenu
    global yAxisMenu
    global crosshair
    global moveSpeed
    global graphInfoBox
    global displayInfoBox
    
    e = getEvent()
    
    pos = Vector2(0,0)

    #Update cursor position
    if e.getServiceType() == ServiceType.Pointer:
        pos = e.getPosition()
        crosshair.setCenter(Vector2(pos[0], pos[1]))
    
    if e.getServiceType() == ServiceType.Wand:
        screenPosition = CoordinateCalculator()
        wand_pos = e.getPosition()
        wand_orient = e.getOrientation()
        r = Vector3(0.0,0.0,-1.0)
        v = wand_orient * r
        screenPosition.set_position(wand_pos.x, wand_pos.y, wand_pos.z)
        screenPosition.set_orientation(v.x, v.y, v.z)
        screenPosition.calculate()
        x = screenPosition.get_x() * displaySize[0]
        y = screenPosition.get_y() * displaySize[1]
        pos = Vector2(x,y)
        crosshair.setCenter(pos)

    if(e.isKeyDown(ord('o'))):
        sizescale *= 1.01
        scaleSize(sizescale)
    if(e.isKeyDown(ord('l'))):
        sizescale *= 0.99
        scaleSize(sizescale)
#    if(e.isKeyDown(ord('p'))):
#        timeScale *= 2.0
#        print timeScale
#    if(e.isKeyDown(ord('i'))):
#        timeScale *= 1/2.0
#        print timeScale

    if(e.isKeyDown(ord('p'))):
        oscale *= 1.01
        scaleOrbit(oscale)
    if(e.isKeyDown(ord('i'))):
        oscale *= 0.99
        scaleOrbit(oscale)

    if(e.isKeyDown(ord('t'))):
        resetCenterAndCamera()
        
        
    if(e.isKeyDown(ord('v'))):
        # graphMultiple.yOffset += 10
        graphMultiple.xOffset += 10
        graphMultiple.plotObjects()
        
    if(e.isKeyDown(ord('b'))):
        # graphMultiple.yOffset -= 10
        graphMultiple.xOffset -= 10
        graphMultiple.plotObjects()
        
    if(e.isKeyDown(ord('c'))):
        # graphMultiple.yOffset += 10
        graphMultiple.zoom *= 0.9
        graphMultiple.plotObjects()
        
    if(e.isKeyDown(ord('n'))):
        # graphMultiple.yOffset -= 10
        graphMultiple.zoom *= 1.1
        graphMultiple.plotObjects()
        
    if(e.isKeyDown(ord('g'))):
        graphMultiple.setImageSize(1.1)

    if(e.isButtonDown(EventFlags.Button2)):
        if(xAxisMenu.isVisible() or yAxisMenu.isVisible()):
            # Mark the event  as processed.
            e.setProcessed()
    
    if e.getServiceType() == ServiceType.Wand:            
        shouldInfoBeVisible = False
                    
        graphInfoBox.setVisible(False)  
        if(graphMultiple.container.hitTest(pos)):
            for point in graphMultiple.points:
                if point.image.hitTest(pos):
                    if displayInfoBox:
                        shouldInfoBeVisible = True

                    updatePlanetInfoBox(point)
                    crossSize =crosshair.getSize()
                    infoPos = Vector2(pos[0]+crossSize[0]/2,pos[1])
                    graphInfoBox.setPosition(infoPos)


        if shouldInfoBeVisible:
            if not graphInfoBox.isVisible():
                graphInfoBox.setVisible(shouldInfoBeVisible)
        else:
            if graphInfoBox.isVisible():
                graphInfoBox.setVisible(shouldInfoBeVisible)

    if(e.isButtonDown(EventFlags.Button5) or e.isButtonDown(EventFlags.Button1)):
        
#        print "Button 5 Clicked"
#        print e.getPosition()
#        screenPosition = CoordinateCalculator()
#        wand_pos = e.getPosition()
#        wand_orient = e.getOrientation()
#        r = Vector3(0.0,0.0,-1.0)
#        v = wand_orient * r
#        screenPosition.set_position(wand_pos.x, wand_pos.y, wand_pos.z)
#        screenPosition.set_orientation(v.x, v.y, v.z)
#        screenPosition.calculate()
#        x = screenPosition.get_x() * displaySize[0]
#        y = screenPosition.get_y() * displaySize[1]
#        print "X:" , x
#        print "Y:" , y
        
        if(graphMultiple.container.hitTest(pos)):
            for point in graphMultiple.points:
                if point.image.hitTest(pos):
                    changeCurrentlySelectedContainer(getContainerForSystem(point.system))
                    updateCenter()
                    
        if(xLabel.hitTest(pos)):
            print "HIT X LABEL"
            xAxisMenu.placeOnWand(e)
            xAxisMenu.show()
        else:
            if xAxisMenu.isVisible():
                xAxisMenu.hide()
         
        if(yLabel.hitTest(pos)):
            print "HIT Y LABEL"
            yAxisMenu.placeOnWand(e)
            yAxisMenu.show()
        else:
            if yAxisMenu.isVisible():
                yAxisMenu.hide()


        for container in allSmallMultiples:
            if(container.container.isEventInside(e)):
                changeCurrentlySelectedContainer(container)
                print currentSystem.name
                updateCenter()
                break

            if(container.container.hitTest(pos)):
                changeCurrentlySelectedContainer(container)
                print currentSystem.name
                updateCenter()
                break

    #       Graph Movement
    
    if(graphMultiple.container.hitTest(pos)):
        if e.getServiceType() == ServiceType.Wand:
            
            # Grab the analog stick horizontal axis
            analogLR = e.getAxis(0)
            
            # Grab the analog stick vertical axis
            analogUD = e.getAxis(1)
            
            if(e.isButtonDown( EventFlags.ButtonUp )): # D-Pad up
                graphMultiple.zoom *= 1.1
            if(e.isButtonDown( EventFlags.ButtonDown )): # D-Pad down
                graphMultiple.zoom *= 0.9
            
            
            graphMultiple.xOffset += moveSpeed * -analogLR
            graphMultiple.yOffset += moveSpeed * -analogUD
            graphMultiple.plotObjects()
            e.setProcessed()


setEventFunction(onEvent)