
from math import *
from euclid import *
from omega import *
from cyclops import *

from omegaToolkit import *

from utilities import *

class Glyph:
    
    allGlyphs = []
    def __init__(self,image,imageHighlight):
        self.imageHighlight = imageHighlight
        self.image = image


#load all images for glyphs
sphere_blue = loadImage("./graphGlyphs/sphere_blue.png")
sphere_blue_highlight = loadImage("./graphGlyphs/sphere_blue_highlight.png")
glyph = Glyph(sphere_blue,sphere_blue_highlight)
Glyph.allGlyphs.append(glyph)

sphere_green = loadImage("./graphGlyphs/sphere_green.png")
sphere_green_highlight = loadImage("./graphGlyphs/sphere_green_highlight.png")
glyph = Glyph(sphere_green,sphere_green_highlight)
Glyph.allGlyphs.append(glyph)


sphere_red = loadImage("./graphGlyphs/sphere_red.png")
sphere_red_highlight = loadImage("./graphGlyphs/sphere_red_highlight.png")
glyph = Glyph(sphere_red,sphere_red_highlight)
Glyph.allGlyphs.append(glyph)


square_blue = loadImage("./graphGlyphs/square_blue.png")
square_blue_highlight = loadImage("./graphGlyphs/square_blue_highlight.png")
glyph = Glyph(square_blue,square_blue_highlight)
Glyph.allGlyphs.append(glyph)


square_green = loadImage("./graphGlyphs/square_green.png")
square_green_highlight = loadImage("./graphGlyphs/square_green_highlight.png")
glyph = Glyph(square_green,square_green_highlight)
Glyph.allGlyphs.append(glyph)


square_red = loadImage("./graphGlyphs/square_red.png")
square_red_highlight = loadImage("./graphGlyphs/square_red_highlight.png")
glyph = Glyph(square_red,square_red_highlight)
Glyph.allGlyphs.append(glyph)



class GraphPoint():

    def __init__(self):
        self.image = None
        self.system = None
        self.planet = None
        self.star = None
        
        self.X = None
        self.Y = None

class GraphContainer(Container):
     
    defaultSize = 50 #TODO CAVE
#    defaultSize = 10 #Laptop Value
    
    def __init__(self,ContainerLayout,parentContainer):
        self.container = Container.create(ContainerLayout,parentContainer)
        self.systems = []
        self.XString = None
        self.YString = None
        self.XArray = []
        self.YArray = []
        self.points = []
        self.size = None
        
        self.xLabel = None
        self.yLabel = None
        
        self.xLog = False
        self.yLog = False
        
        self.xOffset = 0
        self.yOffset = 0
        self.zoom = 1
        
        self.XString = "radius"
        self.YString = "mass"
        
        self.glyph = Glyph.allGlyphs[0]
        
        self.currentSystem = None

    @staticmethod
    def create(ContainerLayout,parentContainer):
        return GraphContainer(ContainerLayout,parentContainer)
        
        
    def reset(self):
        self.xOffset =0
        self.yOFfset =0
        
    def addSystem(self,system):
        print "Adding system"
        self.systems.append(system)
        for star in system.stars:
            for planet in star.planets:
                print "Added point"
                point = GraphPoint()
                point.planet = planet
                point.system = system
                point.star = star
                point.image = Image.create(self.container)
                # point.image.setScale(1)
                point.image.setData(self.glyph.image)
                point.image.setSize(Vector2(GraphContainer.defaultSize,GraphContainer.defaultSize))
                self.points.append(point)
                
                
    def setImageSize(self,size):
        size = GraphContainer.defaultSize * size
        sizeVec = Vector2(size,size)
        for point in self.points:
            point.image.setSize(sizeVec)
        
    def setYLog(self,asLog):
        self.yLog = asLog > 0
        print self.yLog
        self.plotObjects()
        
    def setXLog(self, asLog):
        self.xLog = asLog > 0
        print self.xLog
        self.plotObjects()
        
    def setGlyph(self,index):
        self.glyph = Glyph.allGlyphs[index]
        self.plotObjects()
        
    def plotObjects(self):
    
        print "Plotting objects"
        
        self.XArray = []
        self.YArray = []
        #Generate arrays based on data value properties selected
 
        
        self.xLabel.setText(self.XString.title() + (" (Log)" if self.xLog == True else ""))
        
        self.yLabel.setText(self.YString.title() + (" (Log)" if self.yLog == True else ""))
        
        for point in self.points:
            try:
                point.X = float(eval("point.planet."+self.XString))
            except:
                point.X = 1
                
            if(self.xLog):
                try: point.X = math.log(point.X)
                except: point.X = 0
        
                
            try:
                point.Y = float(eval("point.planet."+self.YString))
            except:
                point.Y = 1
                
            if(self.yLog):
                try: point.Y = math.log(point.Y)
                except: point.Y = 0

            self.XArray.append(point)
            self.YArray.append(point)
                
         #Find Max Values
        maxY = sorted(self.YArray,key=lambda graphPoint : graphPoint.Y)[len(self.YArray)-1].Y
        maxX = sorted(self.XArray,key=lambda graphPoint : graphPoint.X)[len(self.XArray)-1].X
        
        minY = sorted(self.YArray,key=lambda graphPoint : graphPoint.Y)[0].Y
        minX = sorted(self.XArray,key=lambda graphPoint : graphPoint.X)[0].X
        
        

        rangeY = maxY - minY        
        rangeX = maxX - minX
        
        if rangeX == 0:
            rangeX = 1;
            
        if rangeY == 0:
            rangeY = 1

        
        containerSize = self.container.getSize()
                
        # If Log calculate offset
        if(self.xLog):
            logOffsetX = (abs(minX)*self.zoom/rangeX)*containerSize[0]
        else:
            logOffsetX = 0
            
        if(self.yLog):
            logOffsetY = (abs(minY)*self.zoom/rangeY)*containerSize[1]
        else:
            logOffsetY = 0

         
         
        lastToDraw = []
        #Plot arrays
        for point in self.points:
            print point.X
            if point.system == self.currentSystem:
                size = point.image.getSize()
                point.image.setData(self.glyph.imageHighlight)
                point.image.setSize(size)
                lastToDraw.append(point)
            else:
                size = point.image.getSize()
                point.image.setData(self.glyph.image)
                point.image.setSize(size)
                
            point.image.setCenter(Vector2( ((point.X*self.zoom/rangeX)*containerSize[0])+self.xOffset+logOffsetX,(containerSize[1]-((point.Y*self.zoom/rangeY)*containerSize[1]))+self.yOffset-logOffsetY))
            
        #Pseudo sort so highlighted points are on top
        for point in lastToDraw:
            self.container.removeChild(point.image)
            self.container.addChild(point.image)
            # point.image.setCenter(Vector2((point.X*self.zoom/maxX)*containerSize[0]+self.xOffset,containerSize[1]-((point.Y*self.zoom/maxY)*containerSize[1])+self.yOffset))
            