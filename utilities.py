def delete(node):
    numChildren = node.numChildren()
    for i in xrange(numChildren):
        child = node.getChildByIndex(0)
        delete(child)
        node.removeChildByIndex(0)


def clearContainer(container):
	for i in range(container.getNumChildren()):
		container.removeChild(container.getChildByIndex(0))