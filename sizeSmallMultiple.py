# basic omegalib script: just display a textured spinning cube.
from omega import *
from cyclops import *

from systemClasses import *
from systemParse import *

#box = BoxShape.create(0.8, 0.8, 0.8)
#box.setPosition(Vector3(0, 2, -3))
#
## Apply an emissive textured effect (no lighting)
#box.setEffect("textured -v emissive -d cyclops/test/omega-transparent.png")

universe = ParseAll("./system_data")

node = None

system =universe.systems[0]    
node = system.getSizeSmallMultiple(False)
node.setPosition(Vector3(0, 2, -3))

## Spin the box!
#def onUpdate(frame, t, dt):
#	box.pitch(dt)
#	box.yaw(dt / 3)
#setUpdateFunction(onUpdate)
userScaleFactor = 1

def handleEvent():
    global userScaleFactor
    global node
    global system
    
    e = getEvent()
    if(e.isButtonDown(EventFlags.ButtonLeft)):
        print("Left button pressed")
        userScaleFactor = userScaleFactor * 0.75
        node.setPosition(Vector3(0, 2, -userScaleFactor))
    
    if(e.isButtonDown(EventFlags.ButtonRight)):
        print("Right button pressed")
        userScaleFactor = userScaleFactor * 1.25
        node.setPosition(Vector3(0, 2, -userScaleFactor))
#    if(e.isButtonDown(EventFlags.ButtonDown)):
#        print("Down button pressed")
#        node = None
##        node = system.getSizeSmallMultiple(True)
##        node.setPosition(Vector3(0, 2, -3))
#
#    if(e.isButtonDown(EventFlags.ButtonUp)):
#        print("Up button pressed")
#        node = None
##        node = system.getSizeSmallMultiple(False)
##        node.setPosition(Vector3(0, 2, -3))


setEventFunction(handleEvent)
